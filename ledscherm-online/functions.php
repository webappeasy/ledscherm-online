<?php
 
function custom_theme_assets() {
    
    wp_enqueue_script( 'jquery' );
    wp_enqueue_style( 'owlcarousel-style', get_template_directory_uri() . '/includes/owlcarousel/dist/assets/owl.carousel.min.css' );
    wp_enqueue_style( 'owlcarousel-style2', get_template_directory_uri() . '/includes/owlcarousel/dist/assets/animate.css' );
    wp_enqueue_style( 'owlcarousel-theme', get_template_directory_uri() . '/includes/owlcarousel/dist/assets/owl.theme.default.min.css' );
    wp_enqueue_style( 'bootstrap-style', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css', array(), null );
    wp_enqueue_script( 'owlcarousel-script' , get_template_directory_uri() . '/includes/owlcarousel/dist/owl.carousel.min.js' , NULL , false , true );
    wp_enqueue_script( 'slides-script' , get_template_directory_uri() . '/js/slides.js' , NULL , false , true );
    wp_enqueue_script( 'home-script' , get_template_directory_uri() . '/js/home.js' , NULL , false , true );
    wp_enqueue_script( 'bootstrap-script', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js' , NULL , false , true );
    wp_enqueue_script( 'bootstrap-script2', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js' , NULL , false , true );
    wp_enqueue_script( 'lightbox-script', 'https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js' , NULL , false , true );
    wp_enqueue_script( 'popper-script', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js' , NULL , false , true );
    wp_enqueue_style( 'style', get_stylesheet_uri(), array(),time());

    // Localized scripts
    if(is_singular('slideshows')) {
        wp_localize_script('slides-script', 'slideData', array(
            'root' => get_rest_url(),
            'slide' => get_the_ID(),
            'lastModified' => get_the_modified_date('j F Y H:i:s')
        ));
    }
}
 
add_action( 'wp_enqueue_scripts', 'custom_theme_assets' );

// Add custom routes
require get_stylesheet_directory() . '/assets/alert-route.php';

/* Add Featured Image Support To Your WordPress Theme */
function add_featured_image_support_to_your_wordpress_theme() {
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'small-thumbnail', 100, 100, true );
    add_image_size( 'single-post-image', 250, 250, true );
    add_image_size( 'renesse-tf', 144, 166, 2);
    add_image_size( 'zierikzee-gh', 244, 160, true );
    add_image_size( 'zierikzee-bp', 352, 224, true );
}
 
add_action( 'after_setup_theme', 'add_featured_image_support_to_your_wordpress_theme' );
@ini_set( 'upload_max_size' , '48M' );
@ini_set( 'post_max_size', '48M');
@ini_set( 'max_execution_time', '300' );

// Our custom post type function
function create_posttype() {
 
    register_post_type( 'slideshows',
        array(
            'labels' => array(
                'name' => __( 'Slideshows' ), 'singular_name' => __( 'Slideshow' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'slideshow'),
            'capability_type' => 'slideshow',
            'map_meta_cap' => true,
            'menu_icon' => 'dashicons-format-gallery',
        )
    );
    /*register_post_type( 'videos',
        array(
            'labels' => array(
                'name' => __( 'Videos' ), 'singular_name' => __( 'Video' ),
            ),
            'public' => true,
            'has_archive' => true,
            'menu_icon' => 'dashicons-admin-collapse',
        )
    );*/
    
}
// Hooking up our function the theme
add_action( 'init', 'create_posttype' );
        
// HEX to rgba converter
function hex2rgba( $color, $opacity = false ) {

    $default = 'rgb( 0, 0, 0 )';

    /**
     * Return default if no color provided
     */
    if( empty( $color ) ) {

        return $default;

    }

    /**
     * Sanitize $color if "#" is provided
     */
    if ( $color[0] == '#' ) {

        $color = substr( $color, 1 );

    }

    /**
     * Check if color has 6 or 3 characters and get values
     */
    if ( strlen($color) == 6 ) {

        $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );

    } elseif ( strlen( $color ) == 3 ) {

        $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );

    } else {

        return $default;

    }

    /**
     * [$rgb description]
     * @var array
     */
    $rgb =  array_map( 'hexdec', $hex );

    /**
     * Check if opacity is set(rgba or rgb)
     */
    if( $opacity ) {

        if( abs( $opacity ) > 1 )

            $opacity = 1.0;

        $output = 'rgba( ' . implode( "," ,$rgb ) . ',' . $opacity . ' )';

    } else {

        $output = 'rgb( ' . implode( "," , $rgb ) . ' )';

    }

    /**
     * Return rgb(a) color string
     */
    return $output;
}

add_action( 'admin_menu', 'my_admin_menu' );

// Small plugin for easy downloading of the manual
function my_admin_menu() {
    add_menu_page( 'Ledscherm.online handleiding', 'Handleiding', 'subscriber', 'myplugin/myplugin-admin-page.php', 'myplguin_admin_page', 'dashicons-media-text', 6  );
}
function myplguin_admin_page(){
    ?>
    <div class="wrap">
        <a href="https://www.ledscherm.online/Handleiding-Ledscherm-Online.pdf" target="_blank"><h1>Handleiding(klik hier)</h1></a>
    </div>
    <?php
}

// Limit media library access
add_filter( 'ajax_query_attachments_args', 'wpb_show_current_user_attachments' );
 
function wpb_show_current_user_attachments( $query ) {
    $user_id = get_current_user_id();
    if ( $user_id && !current_user_can('activate_plugins') && !current_user_can('edit_others_posts
') ) {
        $query['author'] = $user_id;
    }
    return $query;
} 

// Hide slug editor for posts
function hide_slug_box() {
    global $post;
    global $pagenow;
    if (is_admin() && $pagenow=='post-new.php' OR $pagenow=='post.php') {
        echo "<script type='text/javascript'>
            jQuery(document).ready(function($) {
                jQuery('#edit-slug-box').hide();
            });
            </script>
        ";
    }
}
add_action( 'admin_head', 'hide_slug_box'  );

// Remove quick edit capabilities
add_filter( 'post_row_actions', 'my_disable_quick_edit', 10, 2 );
add_filter( 'page_row_actions', 'my_disable_quick_edit', 10, 2 );

function my_disable_quick_edit( $actions = array(), $post = null ) {

    // Remove the Quick Edit link
    if ( isset( $actions['inline hide-if-no-js'] ) ) {
        unset( $actions['inline hide-if-no-js'] );
    }

    // Return the set of links without Quick Edit
    return $actions;
}

// ACF LOCAL FIELD GROUP
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_5e43b6074ff44',
        'title' => 'Slideshow',
        'fields' => array(
            array(
                'key' => 'field_5e81fdf53225e',
                'label' => 'Slides',
                'name' => '',
                'type' => 'tab',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'placement' => 'top',
                'endpoint' => 0,
            ),
            array(
                'key' => 'field_5e43d90cc812b',
                'label' => 'Slides',
                'name' => 'slide',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => 0,
                'max' => 0,
                'layout' => 'table',
                'button_label' => 'Nieuwe slide',
                'sub_fields' => array(
                    array(
                        'key' => 'field_5e43d9956a13a',
                        'label' => 'Categorie',
                        'name' => 'categorie',
                        'type' => 'select',
                        'instructions' => 'Kies een categorie',
                        'required' => 1,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'choices' => array(
                            'activiteit' => 'Activiteit',
                            'advertentie' => 'Advertentie',
                            'evenement' => 'Evenement',
                            'algemenemelding' => 'Algemene Melding',
                            'noodmelding' => 'Noodmelding',
                        ),
                        'default_value' => false,
                        'allow_null' => 0,
                        'multiple' => 0,
                        'ui' => 0,
                        'return_format' => 'array',
                        'ajax' => 0,
                        'placeholder' => '',
                    ),
                    array(
                        'key' => 'field_5e43d92f6a138',
                        'label' => 'Titel',
                        'name' => 'titel',
                        'type' => 'text',
                        'instructions' => 'Vul hier de titel van de slide in.',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'field_5e43d9956a13a',
                                    'operator' => '!=',
                                    'value' => 'advertentie',
                                ),
                            ),
                        ),
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => 'Optioneel',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5e43d9596a139',
                        'label' => 'Afbeelding',
                        'name' => 'afbeelding',
                        'type' => 'image',
                        'instructions' => 'De afbeelding als achtergrond van de slide.',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'return_format' => 'url',
                        'preview_size' => 'zierikzee-gh',
                        'library' => 'all',
                        'min_width' => '',
                        'min_height' => '',
                        'min_size' => '',
                        'max_width' => '',
                        'max_height' => '',
                        'max_size' => '',
                        'mime_types' => '',
                    ),
                    array(
                        'key' => 'field_5e43d9e26a13b',
                        'label' => 'Datum',
                        'name' => 'datum',
                        'type' => 'date_picker',
                        'instructions' => 'De datum die zichtbaar zijn in de slide.',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'field_5e43d9956a13a',
                                    'operator' => '==',
                                    'value' => 'evenement',
                                ),
                            ),
                        ),
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'display_format' => 'd M',
                        'return_format' => 'Ymd',
                        'first_day' => 1,
                    ),
                    array(
                        'key' => 'field_5e4e645fd58a3',
                        'label' => 'Tijd',
                        'name' => 'tijd',
                        'type' => 'time_picker',
                        'instructions' => 'Vul hier de begin tijd in.',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'field_5e43d9956a13a',
                                    'operator' => '==',
                                    'value' => 'evenement',
                                ),
                            ),
                        ),
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'display_format' => 'H:i',
                        'return_format' => 'H:i',
                    ),
                    array(
                        'key' => 'field_5e82e1d2cea3b',
                        'label' => 'Locatie',
                        'name' => 'locatie',
                        'type' => 'text',
                        'instructions' => 'Vul hier de locatie in.',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'field_5e43d9956a13a',
                                    'operator' => '==',
                                    'value' => 'evenement',
                                ),
                            ),
                        ),
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5e820388416d5',
                        'label' => 'Tekst regel',
                        'name' => 'tekst_regel',
                        'type' => 'text',
                        'instructions' => 'Vul hier je tekst in.',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'field_5e43d9956a13a',
                                    'operator' => '==',
                                    'value' => 'activiteit',
                                ),
                            ),
                        ),
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5e43da626a13d',
                        'label' => 'Publicatie Datum',
                        'name' => 'publicatie_datum',
                        'type' => 'group',
                        'instructions' => 'Van welke tot welke datum is de slide zichtbaar.',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'field_5e43d9956a13a',
                                    'operator' => '!=',
                                    'value' => 'noodmelding',
                                ),
                            ),
                        ),
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'layout' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5e43da8c6a13e',
                                'label' => 'Begindatum',
                                'name' => 'begindatum',
                                'type' => 'date_picker',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'display_format' => 'd-m-Y',
                                'return_format' => 'Ymd',
                                'first_day' => 1,
                            ),
                            array(
                                'key' => 'field_5e43dabf6a13f',
                                'label' => 'Einddatum',
                                'name' => 'einddatum',
                                'type' => 'date_picker',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'display_format' => 'd-m-Y',
                                'return_format' => 'Ymd',
                                'first_day' => 1,
                            ),
                        ),
                    ),
                    array(
                        'key' => 'field_5e43daf96a140',
                        'label' => 'Publicatie Tijd',
                        'name' => 'publicatie_tijd',
                        'type' => 'group',
                        'instructions' => 'Op welke tijden is de slide zichtbaar.',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'field_5e81ff7617516',
                                    'operator' => '==',
                                    'value' => '1',
                                ),
                                array(
                                    'field' => 'field_5e43d9956a13a',
                                    'operator' => '!=',
                                    'value' => 'noodmelding',
                                ),
                            ),
                        ),
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'layout' => 'block',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_5e43daf96a141',
                                'label' => 'Begintijd',
                                'name' => 'begintijd',
                                'type' => 'time_picker',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'display_format' => 'H:i',
                                'return_format' => 'H:i',
                            ),
                            array(
                                'key' => 'field_5e43daf96a142',
                                'label' => 'Eindtijd',
                                'name' => 'eindtijd',
                                'type' => 'time_picker',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'display_format' => 'H:i',
                                'return_format' => 'H:i',
                            ),
                        ),
                    ),
                ),
            ),
            array(
                'key' => 'field_5e81ff233f973',
                'label' => 'Opmaak opties',
                'name' => '',
                'type' => 'tab',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'placement' => 'top',
                'endpoint' => 0,
            ),
            array(
                'key' => 'field_5f16dd2be555c',
                'label' => 'Noodmelding',
                'name' => 'emergency',
                'type' => 'true_false',
                'instructions' => 'Gebruik deze knop als <b>alleen de noodmelding</b> zichtbaar moet zijn.<br> Zorg wel dat je <b>ten minste 1 slide</b> hebt in de categorie <b>"Noodmelding".</b>',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 0,
                'ui' => 0,
                'ui_on_text' => '',
                'ui_off_text' => '',
            ),
            array(
                'key' => 'field_5e846929a8d3f',
                'label' => 'Scherminstellingen',
                'name' => 'scherminstellingen',
                'type' => 'group',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'layout' => 'row',
                'sub_fields' => array(
                    array(
                        'key' => 'field_5e81ff7617516',
                        'label' => 'Publicatie opties',
                        'name' => 'publicatie_opties',
                        'type' => 'true_false',
                        'instructions' => 'Wil je een publicatie tijd?',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'message' => '',
                        'default_value' => 0,
                        'ui' => 0,
                        'ui_on_text' => '',
                        'ui_off_text' => '',
                    ),
                    array(
                        'key' => 'field_5e69dea8fc1f7',
                        'label' => 'Breedte',
                        'name' => 'breedte',
                        'type' => 'number',
                        'instructions' => 'Geef de breedte van het scherm aan.',
                        'required' => 1,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'min' => '',
                        'max' => '',
                        'step' => '',
                    ),
                    array(
                        'key' => 'field_5e69df83fc1f8',
                        'label' => 'Hoogte',
                        'name' => 'hoogte',
                        'type' => 'number',
                        'instructions' => 'Geef de hoogte van het scherm aan.',
                        'required' => 1,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'min' => '',
                        'max' => '',
                        'step' => '',
                    ),
                    array(
                        'key' => 'field_5f5a2a4e2ff7d',
                        'label' => 'Slide tijd',
                        'name' => 'slide_tijd',
                        'type' => 'number',
                        'instructions' => 'Vul hier de tijd in van de overgang tussen slides. Keuze uit 1 t/m 10 seconden.',
                        'required' => 1,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => 5,
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'min' => 1,
                        'max' => 10,
                        'step' => '',
                    ),
                ),
            ),
            array(
                'key' => 'field_5e84673d4bb6b',
                'label' => 'Tekstinstellingen',
                'name' => 'tekstinstellingen',
                'type' => 'group',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'layout' => 'row',
                'sub_fields' => array(
                    array(
                        'key' => 'field_5e8482d1bce7b',
                        'label' => 'Accentkleur Datum',
                        'name' => 'accentkleur_datum',
                        'type' => 'color_picker',
                        'instructions' => 'Geef hier de accentkleur van de datum aan.',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '#ee9720',
                    ),
                    array(
                        'key' => 'field_5e846a94bc1fe',
                        'label' => 'Accentkleur Titel',
                        'name' => 'accentkleur_titel',
                        'type' => 'color_picker',
                        'instructions' => 'Geef hier de Accentkleur van de Titel aan.',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '#ee9720',
                    ),
                    array(
                        'key' => 'field_5e846ad4bc1ff',
                        'label' => 'Accentkleur Tijd, Locatie & tekst',
                        'name' => 'accentkleur_tijd&locatie',
                        'type' => 'color_picker',
                        'instructions' => 'Geef hier de accentkleur van de Tijd, Locatie en tekst aan.',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '#ee9720',
                    ),
                    array(
                        'key' => 'field_5ef312fc3a5e3',
                        'label' => 'Tekstkleur Datum',
                        'name' => 'tekstkleur_datum',
                        'type' => 'color_picker',
                        'instructions' => 'Geef hier de tekstkleur van de datum aan.',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '#FFFFFF',
                    ),
                    array(
                        'key' => 'field_5ef5cc40b552a',
                        'label' => 'Tekstkleur Titel',
                        'name' => 'tekstkleur_titel',
                        'type' => 'color_picker',
                        'instructions' => 'Geef hier de tekstkleur van de titel aan.',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '#FFFFFF',
                    ),
                    array(
                        'key' => 'field_5ef5cc4fb552b',
                        'label' => 'Tekstkleur Tijd, Locatie & Tekst',
                        'name' => 'tekstkleur_tijd&locatie',
                        'type' => 'color_picker',
                        'instructions' => 'Geef hier de tekstkleur van de tijd locatie en tekst aan.',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '#FFFFFF',
                    ),
                    array(
                        'key' => 'field_5e69e104fc1fa',
                        'label' => 'Text',
                        'name' => 'text',
                        'type' => 'range',
                        'instructions' => 'Geef hier de grootte van de text aan in pixels.',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => 20,
                        'min' => 10,
                        'max' => 200,
                        'step' => '',
                        'prepend' => '',
                        'append' => 'px',
                    ),
                    array(
                        'key' => 'field_5e8466a4d8ae4',
                        'label' => 'Hoekjes',
                        'name' => 'hoekjes',
                        'type' => 'range',
                        'instructions' => 'Geef hier de radius van de hoekjes aan.',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => 5,
                        'min' => '',
                        'max' => 50,
                        'step' => '',
                        'prepend' => '',
                        'append' => 'px',
                    ),
                ),
            ),
            array(
                'key' => 'field_5e8206a54f730',
                'label' => 'Instructies',
                'name' => '',
                'type' => 'tab',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'placement' => 'top',
                'endpoint' => 0,
            ),
            array(
                'key' => 'field_5e820a0fa83fc',
                'label' => 'Instructie',
                'name' => 'instructie',
                'type' => 'repeater',
                'instructions' => '<h1>Instructie Slideshow<h1><br><a href="https://www.ledscherm.online/Handleiding-Ledscherm-Online.pdf" target="_blank"><h1>Handleiding(klik hier)</h1></a>',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => 0,
                'max' => 0,
                'layout' => 'table',
                'button_label' => '',
                'sub_fields' => array(
                ),
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'slideshows',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => array(
            0 => 'the_content',
        ),
        'active' => true,
        'description' => '',
    ));
    
    endif;
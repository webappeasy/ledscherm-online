<body id="documentation">
    <div class="cover-container d-flex w-100 p-3 mx-auto flex-column">
        <?php get_header(); ?>
        <main role="main" class="inner cover text-center pt-5 mt-5">
            <div class="mb-5 pb-5 text-white">
                <h1 class="cover-heading">Documentatie</h1>
                <p class="lead">Goed geprogrammeerde software hoeft niet moeilijk te zijn.</p>
            </div>
            <div class="cover-container container pt-5 mb-5 bg-light text-dark rounded-sm">
                <div class="row pl-5 pr-5">
                    <div class="col-md align-self-center text-center">
                        <h2 class="display-5">Introductie</h2>
                        <p class=lead>Slideshow per scherm</p>
                        <p>Per (led)scherm is het mogelijk om een eigen slideshow in te laden. De andere optie is om dezelfde show op meerdere schermen af te spelen.</p>
                    </div>
                    <div class="col-md">
                        <img src="/wp-content/themes/ledscherm-online/includes/owlcarousel/docs/assets/img/mockup-of-a-billboard-sign-from-below-against-a-transparent-background-a15051.png" class="img-fluid" alt="Responsive image">
                    </div>
                </div>
            </div>
            <div class="cover-container container pb-5 pt-5 mb-5 bg-light text-dark rounded-sm">
                <div class="row">
                    <div class="col-md align-self-center text-center pl-5 pr-5">
                        <h2 class="display-5">Slideshows</h2>
                        <p class=lead>Aanpassingen doen</p>
                        <p>Aanpassingen doorvoeren en configureren het van een slideshow is super gemakkelijk. Na een duidelijke uitleg en/of een rondleiding door ons systeem is het voor iedereen mogelijk zijn eigen slideshow te maken. Onze slideshow configuratie heeft een overzichtelijke layout en een duidelijke uitleg bij elk onderdeel.</p>
                        <p class="lead">Kies een categorie</p>
                        <p>Bij elke slide is er een mogelijkheid om een categorie te kiezen. Er is keuze uit Activiteit, Advertentie, Evenement, Algemene melding en Noodmelding. Elk van deze categorieën komt met zijn eigen opties en mogelijkheden. Hierdoor is het mogelijk je content specifieker te visualiseren.</p>
                    </div>
                </div>
            </div>
            <div class="cover-container container pb-5 pt-5 mb-5 bg-light text-dark rounded-sm">
                <div class="row">
                    <div class="col-md align-self-center text-center pl-5 pr-5">
                        <h2 class="display-5">Scherm instellingen</h2>
                        <p class=lead>Hoogte en breedte</p>
                        <p>Voor elke slideshow kan je een eigen hoogte een breedte invoeren. Door deze functionaliteit is je slideshow altijd mooi uitgelijnd. Dit zorgt er voor dat alles er strak en netjes uit ziet.</p>
                        <p class="lead">Accentkleur en formaat</p>
                        <p>Uiteraard is het mogelijk tekst toe te voegen in je slideshow. Om de tekst beter naar voren te laten komen kan je een accentkleur toevoegen. Door de accentkleur rondom te tekst is alles stukken beter leesbaar. En omdat het formaat per slideshow varieert kan je ook de grootte van de tekst aanpassen.</p>
                        <p class=lead>Noodmeldingen</p>
                        <p>Sinds kort is er een nieuwe functie ontstaan binnen ledscherm.online. De noodmelding. Deze kan gebruikt worden wanneer er een noodgeval is of wanneer je de aandacht van mensen wilt trekken. Bij het activeren van deze functie zijn de slides met de categorie noodmelding de enige zichtbare slides.</p>
                    </div>
                </div>
            </div>
        </main>

        <footer class="mastfoot mt-auto text-center">
            <div class="inner">
                <p>&copy; 2020 Ledscherm.online is een dienst van <a href="https://www.webandappeasy.com" target="_blank">Web & App Easy B.V.</a></p>
            </div>
        </footer>
    </div>
    <script>
        jQuery(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                jQuery(this).ekkoLightbox();
            });
    </script>
</body>
<!-- Footer -->
<?php include_once 'footer.php';?>

<?php 
wp_head();
?>
<head><?php
    // Publication option true/false
    $publication = get_field('publicatie_opties');?>
</head><?php

// Emergency option true/false
$emergency = get_field('emergency'); 

// Styling options for users.
$textpreferences = get_field('field_5e84673d4bb6b');
$dateaccentcolor = $textpreferences['accentkleur_datum'];
$titleaccentcolor = $textpreferences['accentkleur_titel'];
$timelocationaccentcolor = $textpreferences['accentkleur_tijd&locatie'];
$dateColor = $textpreferences['tekstkleur_datum'];
$titleColor = $textpreferences['tekstkleur_titel'];
$timelocationColor = $textpreferences['tekstkleur_tijd&locatie'];
$size = $textpreferences['text'];
$border = $textpreferences['hoekjes'];
$color = get_field('field_5e8482d1bce7b'); 
$rgba = hex2rgba( $dateaccentcolor, 0.8 );
$rgba2 = hex2rgba( $titleaccentcolor, 0.8 );
$rgba3 = hex2rgba( $timelocationaccentcolor, 0.8 );

// Current time and date.
$datenow = date_i18n("Ymd"); 
$timenow = date_i18n("H:i"); 

// Slideshow size information.
$sizeGroup = get_field('field_5e846929a8d3f');
$option = $sizeGroup['publicatie_opties'];
$width = $sizeGroup['breedte'];
$height = $sizeGroup['hoogte'];
$slideTime = $sizeGroup['slide_tijd'];


// OwlCarousel Javascript?>
<script>
jQuery(document).ready(function ($) {

    if ($('.owl-carousel').length) {

        // Owl Carousel
        $('.owl-carousel').owlCarousel({
            items: 1,
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: <?php echo $slideTime; ?>000,
            animateOut: 'fadeOutRight',
            animateIn: 'fadeInLeft',
        });
        $('.play').on('click', function () {
            owl.trigger('play.owl.autoplay', [1000])
        })
        $('.stop').on('click', function () {
            owl.trigger('stop.owl.autoplay')
        })
    }
})
</script><?php

// Checking if the emergency field is checked
if($emergency == '1'){?>

    <body class="single-slideshows">

        <div class="owl-carousel" style="width:<?php echo $width?>px;"><?php
            if ( have_posts() ) :
                while ( have_posts() ) : the_post();

                    // retrieve info from the headgroup
                    if( have_rows('field_5e43d90cc812b') ): 
                        while ( have_rows('field_5e43d90cc812b') ) : the_row();

                            // Getting the slide data.
                            $category = get_sub_field('field_5e43d9956a13a');
                            $sub_value = get_sub_field('field_5e43d92f6a138');
                            $image = get_sub_field('field_5e43d9596a139');
                            
                            // post content
                            if($category['value'] == "noodmelding"){?>
                                <div class="image" style="background-image:url(<?php echo $image ?>); height: <?php echo $height?>px; ">
                                    <div class="content" style="height: <?php echo $height?>px;">
                                        
                                        <div class="bottom_information">
                                            <div class="caption" style="color:<?php echo $titleColor ?>; background-color: <?php echo $rgba2?>; border-radius: <?php echo $border?>px;">
                                                <p style="font-size: <?php echo $size ?>pt"><?php echo $sub_value; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div><?php
                            }
                            wp_reset_postdata();
                        endwhile;    
                    endif;
                endwhile;
            endif;?>
        </div>
    </body><?php
}
else {?>
    
    <body class="single-slideshows">
        <div class="owl-carousel" style="width:<?php echo $width?>px;"><?php
            // gather the posts
            if ( have_posts() ) :
                while ( have_posts() ) : the_post();

                    // retrieve info from the headgroup
                    if( have_rows('field_5e43d90cc812b') ): 
                        while ( have_rows('field_5e43d90cc812b') ) : the_row();

                            // Getting the publication times and dates.
                            $dateGroup = get_sub_field('field_5e43da626a13d');
                            $dates = get_sub_field_object($dateGroup);
                            $startDate = $dateGroup['begindatum'];
                            $endDate = $dateGroup['einddatum'];
                            $timeGroup = get_sub_field('field_5e43daf96a140');
                            $times = get_sub_field_object($timeGroup);
                            $startTime = $timeGroup['begintijd'];
                            $endTime = $timeGroup['eindtijd'];
                            $category = get_sub_field('field_5e43d9956a13a');

                            if ($category['value'] != "noodmelding"){
                                // Checking if the post date and time are correct.
                                if ($option == '1' && intval($startDate) <= intval($datenow) && intval($datenow) <= intval($endDate)) {
                                    if ($startTime <= $timenow && $timenow <= $endTime) { 
                                        
                                        // Getting the slide data.
                                        $sub_value = get_sub_field('field_5e43d92f6a138'); // Title
                                        $image = get_sub_field('field_5e43d9596a139');
                                        $date = get_sub_field('field_5e43d9e26a13b');
                                        $time = get_sub_field('field_5e4e645fd58a3');
                                        $location = get_sub_field('field_5e82e1d2cea3b');
                                        $text = get_sub_field('field_5e820388416d5');?>
                                        
                                        <!-- post content -->
                                        <div class="image" style="background-image:url(<?php echo $image ?>); height: <?php echo $height?>px; ">
                                            <div class="content" style="height: <?php echo $height?>px;">
                                                <?php if ($category['value'] == "evenement"):?>
                                                <div class="date" style="background-color: <?php echo $rgba?>; border-radius: 0 0 <?php echo $border?>px <?php echo $border?>px;">
                                                    <p class="datenr" style="color:<?php echo $dateColor ?>; font-size: <?php echo $size ?>pt"><?php echo date('d', strtotime($date)); // number?></p><br>
                                                    <p class="month" style="color:<?php echo $dateColor ?>; font-size: <?php echo $size ?>pt"><?php echo date_i18n('M', strtotime($date)) // month ?></p>
                                                </div>
                                                <div class="bottom_information">
                                                    <div class="caption" style="color:<?php echo $titleColor ?>; background-color: <?php echo $rgba2?>; border-radius: <?php echo $border?>px;">
                                                        <p style="font-size: <?php echo $size ?>pt"><?php echo $sub_value; ?></p>
                                                    </div>
                                                    <div class="caption" style="color:<?php echo $timelocationColor ?>; background-color: <?php echo $rgba3?>; border-radius: <?php echo $border?>px;">
                                                        <p style="font-size: <?php echo $size ?>px">
                                                            <span><?php echo $time; ?></span>
                                                            <?php if($time && $location != ''){ // Divide outputs?>
                                                            <span><?php echo ' | '; ?></span><?php 
                                                            } ?>
                                                            <span><?php echo $location; ?><span>
                                                        </p>
                                                    </div>  
                                                </div>
                                                <?php endif; ?>
                                                <?php if($category['value'] == "activiteit"):?>
                                                <div class="bottom_information">
                                                    <div class="caption" style="color:<?php echo $titleColor ?>; background-color: <?php echo $rgba2?>; border-radius: <?php echo $border?>px;">
                                                        <p style="font-size: <?php echo $size ?>pt"><?php echo $sub_value; ?></p>
                                                    </div><br>
                                                    <div class="caption" style="color:<?php echo $timelocationColor ?>; background-color: <?php echo $rgba3?>; border-radius: <?php echo $border?>px;">
                                                        <p style="font-size: <?php echo $size ?>px"><?php echo $text; ?></p>
                                                    </div>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div><?php
                                    }
                                } 
                                elseif($option == '0' && intval($startDate) <= intval($datenow) && intval($datenow) <= intval($endDate)){ 
                                    $category = get_sub_field('field_5e43d9956a13a');
                                    $sub_value = get_sub_field('field_5e43d92f6a138'); // Title
                                    $image = get_sub_field('field_5e43d9596a139');
                                    $date = get_sub_field('field_5e43d9e26a13b');
                                    $time = get_sub_field('field_5e4e645fd58a3');
                                    $location = get_sub_field('field_5e82e1d2cea3b');
                                    $text = get_sub_field('field_5e820388416d5');?>
                                    
                                    <div class="image" style="background-image:url(<?php echo $image ?>) ; height: <?php echo $height?>px;">
                                        <div class="content" style="height: <?php echo $height?>px;">
                                        <?php if ($category['value'] == "evenement"):?>
                                            <div class="date" style="background-color: <?php echo $rgba?>; border-radius: 0 0 <?php echo $border?>px <?php echo $border?>px;">
                                                <p class="datenr" style="color:<?php echo $dateColor ?>; font-size: <?php echo $size ?>pt"><?php echo date('d', strtotime($date)); // number?></p><br>
                                                <p class="month" style="color:<?php echo $dateColor ?>; font-size: <?php echo $size ?>pt"><?php echo date_i18n('M', strtotime($date)) // month ?></p>
                                            </div>
                                            <div class="bottom_information">
                                                <div class="caption" style="color:<?php echo $titleColor ?>; background-color: <?php echo $rgba2?>; border-radius: <?php echo $border?>px;">
                                                    <p style="font-size: <?php echo $size ?>pt"><?php echo $sub_value; ?></p>
                                                </div>
                                                <div class="caption" style="color:<?php echo $timelocationColor ?>; background-color: <?php echo $rgba3?>; border-radius: <?php echo $border?>px;">
                                                    <p style="font-size: <?php echo $size ?>px">
                                                        <span><?php echo $time; ?></span>
                                                        <?php if($time && $location != ''){ // Divide outputs?>
                                                        <span><?php echo ' | '; ?></span><?php 
                                                        } ?>
                                                        <span><?php echo $location; ?><span>
                                                    </p>
                                                </div>  
                                            </div>
                                        <?php endif; ?>
                                        <?php if($category['value'] == "activiteit"):?>
                                            <div class="bottom_information">
                                                <div class="caption" style="color:<?php echo $titleColor ?>; background-color: <?php echo $rgba2?>; border-radius: <?php echo $border?>px;">
                                                    <p style="font-size: <?php echo $size ?>pt"><?php echo $sub_value; ?></p>
                                                </div><br>
                                                <div class="caption" style="color:<?php echo $timelocationColor ?>; background-color: <?php echo $rgba3?>; border-radius: <?php echo $border?>px;">
                                                    <p style="font-size: <?php echo $size ?>px"><?php echo $text; ?></p>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        </div>
                                    </div><?php 
                                }
                            } 
                            wp_reset_postdata();
                        endwhile;    
                    endif;
                endwhile;
            endif;?>
        </div>
    </body><?php
}

wp_footer();
?>

<body id="contact">
    <div class="cover-container d-flex w-100 p-3 mx-auto flex-column">
        <?php get_header(); ?>
        <main role="main" class="inner cover text-center pt-5 mt-5">
            <div class="mb-5 pb-5 text-white">
                <h1 class="cover-heading">Contact</h1>
                <p class="lead">Neem gerust contact op met ons bij vragen of opmerkingen.</p>
            </div>
            <div class="cover-container container pt-5 mb-5 bg-light text-dark rounded-sm">
                <div class="row pl-5 pr-5">
                    <div class="col-md align-self-center text-center">
                        <h2 class="display-5">Contact gegevens</h2>
                        <p class=lead>Web & App Easy B.V.</p>
                        <p>
                            Roelandsweg 3c<br>
                            4325 CR Renesse<br>
                            0111-407419<br>
                            +31 651 574 660<br>
                            mail@webandappeasy.com
                        </p>
                        <p>
                            Maandag tot met vrijdag geopend van:<br>
                            08.00-17.00 uur.
                        </p>
                    </div>
                    <div class="col-md">
                        <img src="/wp-content/themes/ledscherm-online/includes/owlcarousel/docs/assets/img/mockup-of-a-billboard-sign-from-below-against-a-transparent-background-a15051.png" class="img-fluid" alt="Responsive image">
                    </div>
                </div>
            </div>
        </main>

        <footer class="mastfoot mt-auto text-center">
        <div class="inner">
            <p>&copy; 2020 Ledscherm.online is een dienst van <a href="https://www.webandappeasy.com" target="_blank">Web & App Easy B.V.</a></p>
        </div>
        </footer>
    </div>
    <script>
        jQuery(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                jQuery(this).ekkoLightbox();
            });
    </script>
</body>
<!-- Footer -->
<?php include_once 'footer.php';?>

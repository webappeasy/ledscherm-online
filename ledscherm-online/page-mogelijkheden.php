<body id="features">
    <div class="cover-container d-flex w-100 p-3 mx-auto flex-column">
        <?php get_header(); ?>
        <main role="main" class="inner cover text-center pt-5 mt-5">
            <div class="mb-5 pb-5">
                <h1 class="cover-heading">Alle mogelijkheden</h1>
                <p class="lead">Het online platform voor het aansturen van (led)schermen op zowel klein als groot formaat.</p>
            </div>
            <div class="cover-container container pt-5 mb-5 bg-light text-dark rounded-sm">
                <div class="row">
                    <div class="col-md align-self-center text-center">
                        <h2 class="display-5">Op ieder formaat</h2>
                        <p class=lead>Van heel klein tot heel groot</p>
                        <p>Onze slideshows voldoen gegarandeerd aan je wensen. Ze zitten vol met opties waardoor jouw manier van reclame maken super makkelijk wordt. Zo zijn er zelfs verschillende categorieën te kiezen voor elke slide. Denk hier aan zowel normale advertenties, activiteiten en zelfs evenementen.</p>
                        <p class=lead>Toekomst</p>
                        <p>In de toekomst zullen er nog meer opties en mogelijkheden in onze software worden toegevoegd. De functionaliteit en het uiterlijk kan altijd worden aangepast en verbeterd op de wensen van onze klanten. Uiteraard kan er dus op afspraak <strong>maatwerk</strong> worden geleverd.</p>

                    </div>
                    <div class="col-md">
                        <img src="/wp-content/themes/ledscherm-online/includes/owlcarousel/docs/assets/img/mockup-of-a-billboard-sign-from-below-against-a-transparent-background-a15051.png" class="img-fluid" alt="Responsive image">
                    </div>
                </div>
            </div>
            <div class="cover-container container mt-5 mb-5 pt-5 pb-5">
                <div class="row">
                    <div class="col-md">
                        <h3 class="display-5">Advertenties</h3>
                        <p class="lead">Gemakkelijk op maat</p>
                        <a href="img/advertentie-ledscherm-online.png" data-toggle="lightbox" data-gallery="example-gallery">
                            <img src="/wp-content/themes/ledscherm-online/includes/owlcarousel/docs/assets/img/advertentie-ledscherm-online.png" class="img-fluid rounded-sm shadow-sm" alt="Responsive image">
                        </a>
                    </div>
                    <div class="col-md">
                        <h3 class="display-5">Activiteiten</h3>
                        <p class="lead">Gemakkelijk op maat</p>
                        <a href="img/activiteit-ledscherm-online.png" data-toggle="lightbox" data-gallery="example-gallery">
                            <img src="/wp-content/themes/ledscherm-online/includes/owlcarousel/docs/assets/img/activiteit-ledscherm-online.png" class="img-fluid rounded-sm shadow-sm" alt="Responsive image">
                        </a>
                    </div>
                    <div class="col-md">
                        <h3 class="display-5">Evenementen</h3>
                        <p class="lead">Gemakkelijk op maat</p>
                        <a href="img/evenement-ledscherm-online.png" data-toggle="lightbox" data-gallery="example-gallery">
                            <img src="/wp-content/themes/ledscherm-online/includes/owlcarousel/docs/assets/img/evenement-ledscherm-online.png" class="img-fluid rounded-sm shadow-sm" alt="Responsive image">
                        </a>
                    </div>
                    <div class="col-md">
                        <h3 class="display-5">Noodmeldingen</h3>
                        <p class="lead">Voor elke situatie</p>
                        <a href="img/noodmelding-ledscherm-online.png" data-toggle="lightbox" data-gallery="example-gallery">
                            <img src="/wp-content/themes/ledscherm-online/includes/owlcarousel/docs/assets/img/noodmelding-ledscherm-online.png" class="img-fluid rounded-sm shadow-sm" alt="Responsive image">
                        </a>
                    </div>
                </div>
            </div>
            <div class="cover-container container pb-5 pt-5 mb-5 bg-light text-dark rounded-sm">
                <div class="row">
                    <div class="col-md align-self-center text-center">
                        <h2 class="display-5">Mogelijkheden</h2>
                        <p class=lead>Visueel</p>
                        <p>Er zijn meerdere visuele opties aanwezig voor het maken van de juiste slide. Tot nu toe zijn er opties om een titel, een datum en een tijd toe te voegen. Dit is allemaal optioneel. Want soms zit de informatie al in je achtergrond verwerkt. Uiteraard kan je elk soort afbeelding als achtergrond toevoegen. De afbeelding schaalt zich automatisch op het formaat van het scherm waar je de slideshow voor gebruikt. Je kan namelijk ook elk formaat scherm invullen. Verder is er een accentkleur toegevoegd voor om de tekst heen. Dit is om de tekst beter naar voren te laten komen.</p>
                        <p class="lead">Publicatie</p>
                        <p>In onze software zitten zowel publicatie datums als publicatie tijden. Hier kan je dus instellen op welke dagen en rond welke tijden de slides zichtbaar zijn. Op deze manier kan je je slideshow perfect aanpassen op huidige publiek.</p>
                    </div>
                    <div class="col-md">
                        <a href="/wp-content/themes/ledscherm-online/includes/owlcarousel/docs/assets/img/Schermafbeelding1.png" data-toggle="lightbox">
                            <img src="/wp-content/themes/ledscherm-online/includes/owlcarousel/docs/assets/img/Schermafbeelding1.png" class="img-fluid rounded-md shadow-sm" alt="Responsive image">
                        </a>
                    </div>
                </div>
            </div>
        </main>

        <footer class="mastfoot mt-auto text-center">
            <div class="inner">
                <p>&copy; 2020 Ledscherm.online is een dienst van <a href="https://www.webandappeasy.com" target="_blank">Web & App Easy B.V.</a></p>
            </div>
            </footer>
    </div>
    <script>
        jQuery(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                jQuery(this).ekkoLightbox();
            });
    </script>
</body>
<?php include_once 'footer.php';?>
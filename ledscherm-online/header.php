<!DOCTYPE html>
<html <?php  language_attributes(); ?>>
 
<head>
    <?php wp_head();?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ledscherm.online</title>
</head>

<header class="site-header mb-auto">
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark">
        <div class="container-xl">
            <a class="navbar-brand" href="<?php echo get_home_url(); ?>">Ledscherm.online</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07XL" aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarsExample07XL">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item" role="presentation"><a class="nav-link<?php if (is_page('Home')) echo ' active'; ?>" href="https://www.ledscherm.online/">Home</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link<?php if (is_page('Mogelijkheden')) echo ' active'; ?>" href="https://www.ledscherm.online/mogelijkheden">Mogelijkheden</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link<?php if (is_page('Documentatie')) echo ' active'; ?>" href="https://www.ledscherm.online/documentatie">Documentatie</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link<?php if (is_page('Contact')) echo ' active'; ?>" href="https://www.ledscherm.online/contact">Contact</a></li>
                </ul>
                <?php 
                // Change login button to dashboard when user is already logged in
                if ( is_user_logged_in() ) { ?>
                    <a class="btn btn-outline-light ml-3" role="button" href="/login">Dashboard</a>
                <?php }
                else { ?>
                    <a class="btn btn-outline-light ml-3" role="button" href="/login">Login</a>
                <?php }
                ?>
            </div>
        </div>
    </nav>
</header>

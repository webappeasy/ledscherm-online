<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Create API's for Alerts
add_action('rest_api_init', 'alerts_api');

function alerts_api() {
    register_rest_route('alerts/v1', 'check', array(
        'methods' => WP_REST_SERVER::READABLE,
        'callback' => 'get_alerts'
    ));
}

function get_alerts($request) {

    // If ID is given and is numeric value
    if(isset($_GET['id']) && is_numeric($_GET['id'])) {

        // Get the slideshow by ID and output last edited datetime
        $slideshow = get_post($_GET['id']);

        if($slideshow) {
            return get_the_modified_date('j F Y H:i:s', $slideshow);
        }

        // Show error
        die('Slideshow not found.');

    } else {

        // Show error
        die('No slide ID as parameter found.');
    }
}
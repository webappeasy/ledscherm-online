<?php 
include_once 'header.php';
?>
<h3>ProductKosten</h3>
<tr>
<td>Inkoop</td>
<td>
    <input placeholder="Inhoud" type="value" value="" name="QTY" id="QTY" onKeyUp="divide()" />
</td>
<td>
    <select name="Unit" size="1">
    <option value="gr">Gram</option>
    <option value="Kg">Kilo</option>
    <option value="L">Liter</option>
    <option value="ml">mililiter</option>
    </select>
</td>
<td>
    <input placeholder="Productnaam" type="text" name="product" id="product" />
</td>
<td>
    <input class="euro" type="value" name="PRICE" id="PRICE" onkeyup="divide()" />
</td>
</tr>
<tr>
    </break>
    <h3>ReceptPrijs</h3>
    <td>Gerecht</td>
    <td>
        <input style="display:none" type="text" name="TOTAL" id="TOTAL"  />
    </td>
    <td>
    <input placeholder="ReceptInhoud" type="value" name="RecipeVolume" id="RecipeVolume" onkeyup="multiply()" />
    </td>
    <td>
        <select name="Unit" size="1">
        <option value="gram">Gram</option>
        <option value="kilogram">Kilo</option>
        <option value="liter">Liter</option>
        <option value="mililiter">mililiter</option>
    </select>
    </td>
    <td>
        <input placeholder="Receptprijs" type="value" name="RecipePrice" id="RecipePrice" />
    </td>
</tr>
<break>
<break>

<?php 
if (isset($_POST['submit'])) {
    switch ($_POST['convert']) {
        case "gr-kg":
            $result = gram_to_kilogram($_POST['value']);
            $old_unit = 'gram';
            $new_unit = ' kiloGram';
            break;
        case "kg-gr":
            $result = kilogram_to_gram($_POST['value']);
            $old_unit = ' kiloGram';
            $new_unit = 'gram';
            break;
        case "ml-l":
            $result = mililiter_to_liter($_POST['value']);
            $old_unit = 'mililiter';
            $new_unit = 'liter';
            break;
        case "l-ml":
            $result = liter_to_mililiter($_POST['value']);
            $old_unit = 'liter';
            $new_unit = 'mililiter';
            break;
    }
}

function gram_to_kilogram($given_value) {
    return $given_value/1000;
}

function kilogram_to_gram($given_value) {
    return $given_value*1000;
}

function mililiter_to_liter($given_value) {
    return $given_value/1000;
}

function liter_to_mililiter($given_value) {
    return $given_value*1000;
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Conversions</title>
    </head>
    <body>
        <form method="post">
            Convert <input type="text" name="value" /><br />
            <select name="convert">
                <option value="">--Select one--</option>
                <optgroup label="Units of Length">
                    <option value="gr-kg">Centimeter to inches</option>
                    <option value="in-cm">Inches to centimeter</option>
                </optgroup>

                <optgroup label="Units of Temperature">
                    <option value="f-c">Farenheit to Celcius</option>
                    <option value="c-f">Celcius to Farenheit</option>
                </optgroup>
            </select>
            <input type="submit" name="submit" value="Convert" />
        </form>

        <?php 
        if (!empty($result))
            echo '<p>'.$_POST['value'].$old_unit.' equals '.$result.$new_unit.'</p>';
        ?>
    </body>
</html>
<?php
include_once 'footer.php';
?>
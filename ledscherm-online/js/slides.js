jQuery(document).ready(function ($) {

    if ($('.owl-carousel').length) {

        // Data refresh if post is edited
        setInterval(function () {
            $.ajax({
                url: slideData.root + 'alerts/v1/check',
                type: 'GET',
                data: {
                    id: slideData.slide
                },
                success: response => {

                    // If Modified date from CDATA not equals the date from response
                    if (response != slideData.lastModified) {

                        // Reload the window
                        location.reload()
                    }
                },
                error: response => {

                    // Show error in the console
                    console.log(response.responseText)
                }
            })
        }, 5000)
    }
})

// Hourly refresh if there is a internetconnection
jQuery(document).ready(function($){ 

    // Time check
    setInterval(function checktime() {
        var status = navigator.onLine;
        var dateTime = new Date();
        var minuteTime = dateTime.getMinutes();

        if (status && minuteTime === 1) {

            // Reload the window
            location.reload()
            console.log('Reload')

        } else {
            console.log('No reload')
        }
    }, 60000);
    
})

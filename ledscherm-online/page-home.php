<body id="homepage" class="text-center">
    <div class="cover-container d-flex w-100 h-10 p-3 mx-auto flex-column">
        <?php get_header(); ?>
    
        <header class="mb-auto">
            
        <main role="main" class="inner cover">
        <h1 class="cover-heading mb-5">Ledscherm.online</h1>
        <p class="lead">Hét online platform voor het aansturen van (led)schermen op zowel klein als groot formaat.</p>
        <p class="lead pb-2">Een slideshow bouwen en beheren was nog nooit zo eenvoudig. Pas gemakkelijk het uiterlijk zelf aan.</p>
        <p class="lead">
            <a href="features.html" class="btn btn-lg btn-secondary">Zie mogelijkheden</a>
        </p>
        </main>

        <footer class="mastfoot mt-auto text-center fixed-bottom">
            <div class="inner">
                <p>&copy; 2020 Ledscherm.online is een dienst van <a href="https://www.webandappeasy.com" target="_blank">Web & App Easy B.V.</a></p>
            </div>
        </footer>
    </div>
</div>
<!-- Footer -->
<?php include_once 'footer.php';?>
</body>

